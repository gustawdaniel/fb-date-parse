import {parser} from '../src'
import dayjs from "dayjs"

describe('Parsing facebook dates should be possible', () => {


    test('21 kwietnia o 16:07', () => {
        expect(parser.toJson('21 kwietnia o 16:07')).toStrictEqual({
            year: new Date().getFullYear(),
            month: 4,
            day: 21,
            hour: 16,
            minute: 7,
        })
    })

    test('Wczoraj o 06:04', () => {
        const yesterday = dayjs().add(-1, 'day')

        expect(parser.toJson('Wczoraj o 06:04')).toStrictEqual({
            year: yesterday.year(),
            month: parseInt(yesterday.format('M')),
            day: yesterday.date(),
            hour: 6,
            minute: 4,
        })
    });

    test('28 marca', () => {
        expect(parser.toJson('28 marca')).toStrictEqual({
            year: new Date().getFullYear(),
            month: 3,
            day: 28
        })
    });

    test('1 godz.', () => {
        const oneHourAgo = dayjs().add(-1, 'hour')

        expect(parser.toJson('1 godz.')).toStrictEqual({
            year: oneHourAgo.year(),
            month: parseInt(oneHourAgo.format('M')),
            day: oneHourAgo.date(),
            hour: oneHourAgo.hour()
        })
    });

    test('1 dni', () => {
        const dayAgo = dayjs().add(-1, 'day')

        expect(parser.toJson('1 dni')).toStrictEqual({
            year: dayAgo.year(),
            month: parseInt(dayAgo.format('M')),
            day: dayAgo.date()
        })
    });

    test('51 sekund temu', () => {
        const ago = dayjs().add(-51, 'second')

        expect(parser.toJson('51 sekund temu')).toStrictEqual({
            year: ago.year(),
            month: parseInt(ago.format('M')),
            day: ago.date(),
            hour: ago.hour(),
            minute: ago.minute(),
            second: ago.second(),
        })
    });

    test('59 min', () => {
        const someMinutesAgo = dayjs().add(-59, 'minutes')

        expect(parser.toJson('59 min')).toStrictEqual({
            year: someMinutesAgo.year(),
            month: parseInt(someMinutesAgo.format('M')),
            day: someMinutesAgo.date(),
            hour: someMinutesAgo.hour(),
            minute: someMinutesAgo.minute()
        })
    });

    test('fromString', () => {
        expect(parser.fromString('28 marca')).toStrictEqual(
            new Date(new Date().getFullYear(), 3 - 1, 28, 0, 0, 0)
        )
    })

    test('31 stycznia 2020', () => {
        expect(parser.fromString('31 stycznia 2020')).toStrictEqual(
            new Date(2020, 0, 31, 0, 0, 0)
        )
    })

    test('19 min', () => {
        const someMinutesAgo = dayjs().add(-19, 'minutes')

        expect(parser.toJson('19 min')).toStrictEqual({
            year: someMinutesAgo.year(),
            month: parseInt(someMinutesAgo.format('M')),
            day: someMinutesAgo.date(),
            hour: someMinutesAgo.hour(),
            minute: someMinutesAgo.minute()
        })

        expect(parser.fromString('19 min')).toStrictEqual(
            someMinutesAgo.set('second', 0).set('millisecond', 0).toDate()
        )
    })

    test('non breaking space', () => {
        const nbsp = ["1", "7", " ", "g", "o", "d", "z", "."].join('');
        const regular = ["1", "7", " ", "g", "o", "d", "z", "."].join('');

        expect(parser.toJson(nbsp)).toStrictEqual(parser.toJson(regular))
        expect(parser.fromString(nbsp)).toStrictEqual(parser.fromString(regular))
    })

    test('5 czerwca o 13:46', () => {
        const expectedDate = new Date(new Date().getFullYear(), 6 - 1, 5, 13, 46, 0);
        expect(parser.fromString('5 czerwca o 13:46')).toStrictEqual(expectedDate);

        const name = [53, 160, 99, 122, 101, 114, 119, 99, 97, 160, 111, 160, 49, 51, 58, 52, 54].map(c => String.fromCharCode(c)).join('');

        expect(name).not.toStrictEqual('5 czerwca o 13:46');

        expect(parser.fromString(name)).toStrictEqual(expectedDate);
    })

    test('relative date is forgotten after usage', () => {
        const refDate = new Date(2002, 5, 0, 0, 0, 0);
        expect(dayjs(parser.setDate(refDate).fromString('3 dni')).year()).toStrictEqual(2002);
        expect(dayjs(parser.fromString('3 dni')).year()).toStrictEqual(new Date().getFullYear());
    });

    test('Wczoraj o 06:04 - reference date', () => {
        const relativeDate = parser.setDate(new Date('2021-06-08T20:17:11.421Z')).toJson('Wczoraj o 06:04');
        expect(relativeDate).toStrictEqual({
            year: 2021,
            month: 6,
            day: 7,
            hour: 6,
            minute: 4
        })
    })

    test('13 listopada o 03:42 - reference date - 2020', () => {
        const relativeDate = parser
            .setDate(new Date('2020-11-16T06:50:30.000Z'))
            .toJson('13 listopada o 03:42');
        expect(relativeDate).toStrictEqual({
            year: 2020,
            month: 11,
            day: 13,
            hour: 3,
            minute: 42
        })
    })

    test('11 października 2018', () => {
        expect(parser.toJson('11 października 2018')).toStrictEqual({
            year: 2018,
            month: 10,
            day: 11
        })
    })

    test('przedwczoraj o 04:39', () => {
        const ago = dayjs().add(-2, 'day').set('hour', 4).set('minute', 39);

        expect(parser.unsetDate().toJson('przedwczoraj o 04:39')).toStrictEqual({
            year: ago.year(),
            month: parseInt(ago.format('M')),
            day: ago.date(),
            hour: 4,
            minute: 39
        })
    })

    test('1 tydzień temu', () => {
        const ago = dayjs().add(-1, 'week');

        expect(parser.toJson('1 tydzień temu')).toStrictEqual({
            year: ago.year(),
            month: parseInt(ago.format('M')),
            day: ago.date()
        })
    })

    test('2 tygodnie temu', () => {
        const ago = dayjs().add(-2, 'weeks');

        expect(parser.toJson('2 tygodnie temu')).toStrictEqual({
            year: ago.year(),
            month: parseInt(ago.format('M')),
            day: ago.date()
        })
    })

    test('3 tygodnie temu', () => {
        const ago = dayjs().add(-3, 'weeks');

        expect(parser.toJson('3 tygodnie temu')).toStrictEqual({
            year: ago.year(),
            month: parseInt(ago.format('M')),
            day: ago.date()
        })
    })

    test('5 tygodni temu', () => {
        const ago = dayjs().add(-5, 'weeks');

        expect(parser.toJson('5 tygodni temu')).toStrictEqual({
            year: ago.year(),
            month: parseInt(ago.format('M')),
            day: ago.date()
        })
    })


    test('dziś o 02:51', () => {
        const now = dayjs();

        expect(parser.toJson('dziś o 02:51')).toStrictEqual({
            year: now.year(),
            month: parseInt(now.format('M')),
            day: now.date(),
            hour: 2,
            minute: 51,
        })
    })

    test('po jutrze o 02:51', () => {
        const now = dayjs().add(2, 'days');

        expect(parser.toJson('po jutrze o 02:51')).toStrictEqual({
            year: now.year(),
            month: parseInt(now.format('M')),
            day: now.date(),
            hour: 2,
            minute: 51,
        })
    })

    test('jutro o 02:51', () => {
        const now = dayjs().add(1, 'day');

        expect(parser.toJson('jutro o 02:51')).toStrictEqual({
            year: now.year(),
            month: parseInt(now.format('M')),
            day: now.date(),
            hour: 2,
            minute: 51,
        })
    })

    test('invalid date', () => {
        const invalid = 'invalid date';
        expect(parser.toJson(invalid)).toStrictEqual({"invalid": `Unable to parse ${invalid}`})
    })

    test('exception replaced by invalid response', () => {
        const invalid = '3 date';
        expect(parser.toJson(invalid)).toStrictEqual({"invalid": `Unable to parse ${invalid}`})
    })
})
