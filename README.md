# FB Date Parser

[![pipeline report](https://gitlab.com/gustawdaniel/fb-date-parse/badges/master/pipeline.svg?key_text=Gitlab+CI&key_width=70)](https://gitlab.com/gustawdaniel/fb-date-parse/-/pipelines)

Plugin for parsing Polish dates from Facebook web page. It allows convert datest like:

- dziś o 02:51
- jutro o 02:51
- po jutrze o 02:51
- przedwczoraj o 04:39
- Wczoraj o 06:04
- 2 tygodnie temu
- 31 stycznia 2020 
- 21 kwietnia o 16:07
- 28 marca
- 1 godz.
- 1 dni
- 59 min
- 51 sekund temu

To JSON format or directly to Date objects.

## Installation

```
npm i fb-date-parser
```

## Usage

Import parser:

```
import {parser} from 'fb-date-parser';
```

You can convert human version of date to JSON:

```
parser.toJson('Wczoraj o 06:04');
```

you will see

```
{year: 2021, month: 4, day: 7, hour: 6, minute: 4}
```

You can also get Date object:

```
parser.fromString('Wczoraj o 06:04');
```

you should get:

```
Wed Apr 07 2021 06:04:00 GMT+0200 (Central European Summer Time)
```

## Relative Dates

If you have date described as `Wczoraj o 06:04` but scrapped at `2021-06-08T20:17:11.421Z`,
you can set relative date by the following code:

```
parser.setDate(new Date('2021-06-08T20:17:11.421Z'))
  .fromString('Wczoraj o 06:04')
```

You will see `new Date('2021-06-07T06:04')`

This relative data will be saved until `fromString` or `toJson` will be used.
Then it is cleared. You can clear date by yourself using `unsetDate()`.

## Sponsors

Package is funded and maintained by company:

- JUPITER19 SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ

## Changelog

### [1.1.7] - 2021-06-09
#### Added
- Added `51 sekund temu`

### [1.1.6] - 2021-06-09
#### Added
- Added `dziś o 02:51`, `jutro`, `po jutrze`

### [1.1.5] - 2021-06-09
#### Added
- Weeks abd a day before yesterday support
#### Changed  
- Relative dates are not saved in parser state
#### Fixed
- Consistent error handling

### [1.1.4] - 2021-06-08
#### Fixed
- Relative dates with years works better
- Months with polish characters fixed

### [1.1.3] - 2021-06-08
#### Fixed
- tscofig.json fixed (dist simplified)

### [1.1.2] - 2021-06-08
#### Fixed
- Dist content added to package

### [1.1.1] - 2021-06-08
#### Added
- Relative dates

### [1.1.0] - 2021-06-08
#### Changed
- Simplified import
- Lib directory renamed to dist

### [1.0.2] - 2021-05-08
#### Added
- Non-breaking spaces handling (NBSP)
- CHANGELOG and documentation
- Test case for `31 stycznia 2020`

### [1.0.1] - 2014-05-08

#### Added
- Pure JS version of library in `lib` directory
- Method `fromString` that returns Date object

#### Changed
- Name of `attempt` changed to `toJson`

### [1.0.0] - 2014-05-08

#### Added
- Method `attempt` that prepare `JSON` object representing date  
- Tests for cases 

```
11 października 2018
21 kwietnia o 16:07
Wczoraj o 06:04
28 marca
1 godz.
1 dni
59 min
```