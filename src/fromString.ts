function fromStringGenerator(parser: any, defaultLocale: string = 'pl') {
    return function fromStringFunction(string: string, locale = defaultLocale) {
        const parsed = parser.toJson(string);
        if (parsed.invalid) {
            return parsed;
        }
        const date = new Date();
        // default to current year, month and day
        if (typeof parsed.year === 'number') {
            date.setFullYear(parsed.year);
        }
        if (typeof parsed.month === 'number') {
            date.setMonth(parsed.month - 1);
        }
        if (typeof parsed.day === 'number') {
            date.setDate(parsed.day);
        }
        // default to first unit for time components
        date.setHours(parsed.hour || 0);
        date.setMinutes(parsed.minute || 0);
        date.setSeconds(parsed.second || 0);
        date.setMilliseconds(parsed.millisecond || 0);
        if (typeof parsed.offset === 'number') {
            return new Date(date.getTime() - parsed.offset * 60 * 1000);
        }
        return date;
    };
}

export {fromStringGenerator};