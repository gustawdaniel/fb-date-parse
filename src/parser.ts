const {Format, Parser} = require('any-date-parser');
import {fromStringGenerator} from './fromString'
import dayjs, {OpUnitType} from "dayjs";

const monthRegex = /(stycznia|lutego|marca|kwietnia|maja|czerwca|lipca|sierpnia|września|października|listopada|grudnia)/;
const unitRegex = /(min|godz|dni|tydzień|tygodnie|tygodni|sekundę|sekundy|sekund)/;

const monthByName: { [key: number]: RegExp } = {
    1: /stycznia/,
    2: /lutego/,
    3: /marca/,
    4: /kwietnia/,
    5: /maja/,
    6: /czerwca/,
    7: /lipca/,
    8: /sierpnia/,
    9: /września/,
    10: /października/,
    11: /listopada/,
    12: /grudnia/,
};

const unitByName: { [key: string]: RegExp } = {
    'week': /tydzień|tygodnie|tygodni/,
    'day': /dni/,
    'hour': /godz/,
    'minute': /min/,
    'second': /sekundę|sekundy|sekund/
};

type PartialRecord<K extends string | number, T> = { [P in K]?: T; };


const getUsingSelector = (source: string, selector: PartialRecord<string | number, RegExp>): OpUnitType | string => {
    let value;

    const valueEntry = Object.entries(selector).find(([key, exp]: [string, RegExp | undefined]) => {
        return exp && exp.test(source)
    })
    if (!valueEntry) throw new Error('Month not recognized');

    if (valueEntry)
        value = valueEntry[0]

    return value as string;
}

const getMonth = (humanMonth: string): string => {
    return getUsingSelector(humanMonth, monthByName) as string
}

const getUnit = (humanUnit: string): OpUnitType => {
    return getUsingSelector(humanUnit, unitByName) as OpUnitType
}

interface FbDateParser {
    addFormat: (format: any) => void,
    toJson: (date: string) => object,
    attempt: (date: string, locale: string) => object,
    fromString: (date: string) => Date
    setDate: (date: Date) => FbDateParser
    unsetDate: () => FbDateParser,
    referenceDate?: Date
}


const parser: FbDateParser = new Parser();

const beforeDays = (before: string):number => {
    switch (true) {
        case /przedwczoraj/i.test(before): return -2;
        case /wczoraj/i.test(before): return -1;
        case /dziś/i.test(before): return 0;
        case /jutro/i.test(before): return 1;
        case /po jutrze/i.test(before): return 2;
        default: throw new Error(`before days: ${before} not recognized`)
    }
}

// wczoraj o 12:12
parser.addFormat(new Format({
    matcher: /^(dziś|przedwczoraj|wczoraj|jutro|po jutrze) o (\d{1,2}):(\d{1,2})$/i,
    handler: ([_, before, hour, minute]: [void, string, number, number]) => {
        const yesterday = dayjs(parser.referenceDate).add(beforeDays(before), 'day')

        return {year: yesterday.year(), month: parseInt(yesterday.format('M')), day: yesterday.date(), hour, minute};
    },
}))

// 11 października o 12:20
parser.addFormat(new Format({
    matcher: /^(\d{1,2}) ([a-zśźń]+) o (\d{1,2}):(\d{1,2})$/,
    handler: ([_, day, humanMonth, hour, minute]: [void, number, string, number, number]) => {
        return {
            year: dayjs(parser.referenceDate).year(),
            month: getMonth(humanMonth),
            day,
            hour,
            minute
        };
    },
}))

// 11 października 2018
parser.addFormat(new Format({
    matcher: /^(\d{1,2}) ([a-zśźń]+) (\d{4})$/,
    handler: ([_, day, humanMonth, year]: [void, number, string, number]) => {
        return {
            year,
            month: getMonth(humanMonth),
            day
        };
    },
}))

// 12 maja | 12 godz. |
parser.addFormat(new Format({
    matcher: /^(\d{1,2}) ([a-zśźń]+).?$/,
    handler: ([_, value, monthOrUnit]: [void, number, string]) => {
        if (monthRegex.test(monthOrUnit)) {
            return {
                year: dayjs(parser.referenceDate).year(),
                month: getMonth(monthOrUnit),
                day: value
            };
        } else if (unitRegex.test(monthOrUnit)) {
            const unit = getUnit(monthOrUnit);
            const ago = dayjs(parser.referenceDate).add(-value, unit);

            return {
                year: ago.year(),
                month: parseInt(ago.format('M')),
                day: ago.date(),
                ...(['hour', 'minute', 'second'].includes(unit || '') ? {hour: ago.hour()} : {}),
                ...(['minute', 'second'].includes(unit || '') ? {minute: ago.minute()} : {}),
                ...(['second'].includes(unit || '') ? {second: ago.second()} : {})
            }
        } else {
            throw new Error(`NOT_KNOWN_VALUE: ${monthOrUnit}`)
        }
    },
}))

const makeSpacesNormal = (str:string):string => str.replace(/(?=\s)[^\r\n\t]/g, ' ');
const normalize = (str:string):string => str.replace(/\s+temu$/,'');

interface FbDateJson {
    invalid?: string
    year?: number,
    month?: number,
    day?: number,
    hour?: number,
    minute?: number,
    second?: number,
}

parser.toJson = function (date: string):FbDateJson {
    let res: FbDateJson;
    try {
        res = this.attempt(normalize(makeSpacesNormal(date)), 'pl') as FbDateJson;
    } catch (e) {
        return {
            invalid: `Unable to parse ${date}`
        }
    } finally {
        this.unsetDate();
    }
    return res;
}
parser.fromString = fromStringGenerator(parser)

parser.referenceDate = undefined
parser.setDate = function (date:Date) {
    this.referenceDate = date;
    return this;
}
parser.unsetDate = function () {
    this.referenceDate = undefined;
    return this;
}

export {parser};